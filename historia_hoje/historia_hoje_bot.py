#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging.handlers
import os
import random
import configparser
import sys

from telegram import Update
from telegram.ext import Application, CommandHandler, ContextTypes, MessageHandler, filters
import googleapiclient.discovery
from googleapiclient.errors import HttpError

import click
import emoji
import openai
from sydney import SydneyClient


HH_LOGGING_LEVEL = int(os.getenv("HH_LOGGING_LEVEL", 10))

logger = logging.getLogger(__name__)
logger.setLevel(HH_LOGGING_LEVEL)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(HH_LOGGING_LEVEL)
S_HANDLER.setFormatter(formatter)
logger.addHandler(S_HANDLER)


ERROR_QUOTES = [
    "Ops! Algo saiu errado! Contate algum humano pra resolver isso.",
    "Desculpe, ocorreu um erro inesperado.",
    "Ops, parece que me ocorreu um erro.",
    "Houve um erro durante o processamento.",
    "Algo deu errado, por favor, tente novamente.",
    "Desculpe pelo incoveniente, detectamos um erro.",
    "Essa resposta não pode ser exibida devido a um erro.",
    "Houve uma falha inesperada. Erro detectado.",
    "Desculpe, o sistema encontrou uma situação anormal.",
    "Algo deu errado, erro detectado.",
    "Lamentamos, mas encontrei um erro no processo.",
]


GREETINGS_QUOTES = [
    "Bem-vindo ao História Hoje {}! O seu destino para explorar os eventos do passado.",
    "Bem-vindo ao História Hoje {}! Aqui, a história ganha vida.",
    "Bem-vindo ao História Hoje {}! Aqui, cada dia é uma nova aula de história.",
    "Bem-vindo ao História Hoje {}! O canal onde o passado se torna presente.",
    "Bem-vindo ao História Hoje {}! Embarque em nossa jornada histórica!",
    "Bem-vindo ao História Hoje {}! O ponto de encontro para entusiastas de história.",
    "Bem-vindo ao História Hoje {}! Seja parte da nossa comunidade de entusiastas da história em História Hoje.",
    "Bem-vindo ao História Hoje {}! Explore conosco os eventos que moldaram o mundo.",
    "Bem-vindo ao História Hoje {}! O seu portal para o passado, presente e futuro.",
    "Bem-vindo ao História Hoje {}! O lar dos apaixonados por passado e presente."
]


GOODBYE_QUOTES = [
    "Adeus, {}! Sentiremos sua falta no grupo. Cuide-se!",
    "Obrigado por fazer parte do nosso grupo, {}. Até mais!",
    "Desejamos a você tudo de bom em sua jornada, {}. Adeus!",
    "A saída de {} deixa um vazio no grupo. Adeus e boa sorte!",
    "Foi ótimo ter você conosco, {}. Até a próxima!",
    "Que sua jornada futura seja cheia de sucessos, {}. Adeus!",
    "Vamos sentir sua falta, {}. Cuide-se e adeus!",
    "Nossa despedida é difícil, {}, mas desejamos o melhor para você!",
    "Você sempre será bem-vindo de volta, {}. Adeus por agora!",
    "Adeus, {}. Esperamos que nos encontremos novamente em breve!",
]


class YoutubeVideo(object):
    def __init__(self, id="", title=""):
        self.id = id
        self.title = title
        self._config = configparser.ConfigParser()
        self._config["video"] = {"id": self.id, "title": self.title}

    def __eq__(self, other):
        return self.id == other.id

    def store(self, json_path):
        with open(json_path, 'w') as fd:
            self._config.write(fd)

    @property
    def link(self):
        return f"https://www.youtube.com/watch?v={self.id}"

    @staticmethod
    def generate(snippet):
        return YoutubeVideo(snippet["resourceId"]["videoId"], snippet["title"])

    @staticmethod
    def restore(json_path):
        try:
            config = configparser.ConfigParser()
            config.read(json_path)
            return YoutubeVideo(config["video"]["id"], config["video"]["title"])
        except Exception:
            return YoutubeVideo()


class YoutubeScrapper:

    def __init__(self, logger, gcp_api_keys):
        self._logger = logger
        self._youtube = None
        self._gcp_api_keys = gcp_api_keys
        self._playlist = None

    def _login_youtube(self, channel_id):
        self._logger.debug("Executing YT login")
        for key in self._gcp_api_keys:
            try:
                self._logger.info("Connecting to YT with key {}****".format(key[:8]))
                self._youtube = googleapiclient.discovery.build("youtube", "v3", developerKey=key)
                request = self._youtube.channels().list(part="id,contentDetails", id=channel_id, maxResults=1)
                response = request.execute()
                if not response:
                    message = "Could not login on Youtube!"
                    self._logger.error(message)
                self._playlist = response["items"][0]["contentDetails"]["relatedPlaylists"]["uploads"]
                self._logger.info(f"Logged-in on youtube, Playlist ID: {self._playlist}")
                return
            except HttpError as err:
                self._logger.error(err.reason)
                pass
        raise Exception("Could not login on YT! Giving up!")

    def get_latest_video(self, channel_id):
        if self._youtube is None:
            self._login_youtube(channel_id)
        request = self._youtube.playlistItems().list(part="id,snippet", playlistId=self._playlist, maxResults=1)
        response = request.execute()
        if not response:
            raise Exception(f"Could not scrap YT channel {channel_id}!")
        snippet = response["items"][0]["snippet"]
        video = YoutubeVideo.generate(snippet)
        return video


def is_under_maintenance():
    return os.getenv("HH_UNDER_MAINTENANCE", False)


def is_dry_run():
    return os.getenv("HH_DRY_RUN", False)


def remove_emojis_from_text(text):
    return emoji.replace_emoji(text, replace='')


async def send_message(context: ContextTypes.DEFAULT_TYPE, chat_id, text: str) -> None:
    logger.debug(f"[{chat_id}]: {text}")
    try:
        await context.bot.send_message(chat_id=chat_id, text=text)
    except Exception as error:
        logger.error(f"Could not post text message: {error}")
        if not is_dry_run():
            try:
                await context.bot.send_message(chat_id=chat_id, text=text)
            except Exception as error:
                logger.error(f"Could not post text message: {error}")


async def greetings(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message = random.choice(GREETINGS_QUOTES)
    cid = update.message.chat.id
    for member in update.message.new_chat_members:
        await send_message(context, chat_id=cid, text=message.format(member.name))


async def goodbye(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message = random.choice(GOODBYE_QUOTES)
    cid = update.message.chat.id
    member = update.message.left_chat_member.name
    await send_message(context, chat_id=cid, text=message.format(member))


class Configuration:
    def __init__(self):
        self.config = self.config_file()

    @property
    def config_file_path(self):
        return os.getenv("HH_CONFIG", "/etc/historia_hoje.conf")

    def config_file(self):
        config_path = self.config_file_path
        if not os.path.exists(config_path):
            raise ValueError("Could not obtain configuration file path.")
        config = configparser.ConfigParser()
        config.read(config_path)
        return config

    @property
    def telegram_token(self):
        return self.config["telegram"]["token"]

    @property
    def telegram_chat_id(self):
        return self.config["telegram"]["chat_id"]

    @property
    def openai_token(self):
        return self.config["openai"]["token"]

    @property
    def youtube_api_keys(self):
        return self.config["youtube"]["gcp_api_keys"].split(',')

    @property
    def youtube_channel_id(self):
        return self.config["youtube"]["channel_id"]

    @property
    def bing_u_cookie(self):
        return self.config["bing"]["cookie"]


def get_chatgpt_answer(message: str) -> str:
    try:
        completion = openai.ChatCompletion.create(model="gpt-3.5-turbo", messages=[
            {"role": "system", "content": "Your are an historician and try to explain directly, in a simple way"},
            {"role": "user", "content": message}], temperature=0.8, max_tokens=100)
        return completion.choices[0].message.content
    except Exception as error:
        logger.error(f'Could not process ChatGPT: {error}')


async def get_bing_answer(message: str) -> str:
    configuration = Configuration()
    # sydney_client = SydneyClient(bing_u_cookie=configuration.bing_u_cookie)
    try:
        answer = ""
        skip_first = True
        async with SydneyClient(bing_u_cookie=configuration.bing_u_cookie) as sydney_client:
            async for response in sydney_client.ask_stream(message):
                if skip_first:
                    # INFO: First response is always: "Searching for answers..."
                    skip_first = False
                    if 'Searching the web for' in response:
                        continue
                answer += response
        return answer
    except Exception as error:
        logger.error(f'Could not process Bing: {error}')
        await sydney_client.reset_conversation()


async def assistant(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message = update.message
    cid = update.message.chat.id
    message_text = None
    # message sent directly to the bot user
    if message.chat.type == 'private':
        message_text = message.text.strip()
    # If in a group, only reply to mentions.
    elif "@historiahojebot" in update.message.text.lower():
        # Strip first word (the mention) from message text.
        message_text = update.message.text.replace('@historiahojebot', '')

    logger.debug(f"rcv msg: {message_text}")

    if message_text:
        answer_text = get_error_message()
        try:
            answer_text = await get_bing_answer(message_text)
            if not answer_text:
                answer_text = get_chatgpt_answer(message.text)
        except Exception as error:
            logger.error(f"Could not get answer: {error}")

        logger.debug(f"answer msg: {answer_text}")
        await send_message(context, cid, answer_text)


def get_error_message():
    return random.choice(ERROR_QUOTES)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id, text='Olá! Procurando melhor canal de história no YT?\nAcesse https://www.youtube.com/@Historia_Hoje')


async def ultimo_episodio(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    configuration = Configuration()
    scrapper = YoutubeScrapper(logger, configuration.youtube_api_keys)
    video = scrapper.get_latest_video(Configuration().youtube_channel_id)
    await send_message(context, chat_id=update.message.chat_id,
                            text="Último video disponível: {} - {}".format(video.title, video.link))


async def no_dia_de_hoje(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    question = 'Cite em PT-BR, sem emojis, um evento histórico que ocorreu no dia de hoje, ou uma grande batalha. O texto deverá começar com "No dia de hoje, mas no ano de".'
    answer = get_chatgpt_answer(question)
    await send_message(context, chat_id=update.message.chat_id, text=answer)


async def ajuda(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Use / pra listar os comandos.")


def error(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.warning(f'Ops! "{update}" deu erro "{context.error}"')


@click.command()
@click.option('--verbose', '-v', is_flag=True, default=False, help='Verbose logging.')
def historia_hoje(verbose, *args, **kwargs):
    configuration = Configuration()

    if configuration.telegram_token is None:
        logger.error("HH TELEGRAM TOKEN is Empty.")
        raise ValueError("HH_TELEGRAM_TOKEN is unset.")

    application = Application.builder().token(configuration.telegram_token).build()

    if is_under_maintenance():
        logging.warning("Historia Hoje is under maintenance. Exiting now.")
        sys.exit(503)

    application.add_handler(CommandHandler('start', start))
    application.add_handler(CommandHandler('ultimoep', ultimo_episodio))
    application.add_handler(CommandHandler('nodiadehoje', no_dia_de_hoje))
    application.add_handler(CommandHandler('ajuda', ajuda))
    application.add_handler(MessageHandler(filters.StatusUpdate.NEW_CHAT_MEMBERS, greetings))
    application.add_handler(MessageHandler(filters.StatusUpdate.LEFT_CHAT_MEMBER, goodbye))
    application.add_error_handler(error)

    openai.api_key = configuration.openai_token

    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, assistant))
    application.run_polling(allowed_updates=Update.ALL_TYPES)


def show_configuration():
    configuration = Configuration()
    logger.info(f"Telegram token: {configuration.telegram_token[:8]}****")
    logger.info(f"Telegram chat_id: {configuration.telegram_chat_id}")
    logger.info(f"OpenAI token: {configuration.openai_token[:8]}****")
    logger.info(f"Bing U Cookie: {configuration.bing_u_cookie[:8]}****")
    logger.info(f"Youtube Channel ID: {configuration.youtube_channel_id}")


def main():
    show_configuration()
    historia_hoje()


if __name__ == '__main__':
    main()